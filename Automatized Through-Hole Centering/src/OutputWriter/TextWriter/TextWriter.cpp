/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   TextWriter
 */


#include "TextWriter.h"

#include <fstream>

TextWriter::TextWriter(const std::string& inBuffer, const TextWriterType inType)
  : _OutputBuffer(inBuffer)
  , _Type(inType)
{
}


TextWriter::~TextWriter()
{
}

void TextWriter::save(const std::string& inFilePath)
{
  std::ofstream outputText;

  if (_Type == TextWriterType::Append)
  {
    outputText.open(inFilePath, std::ios_base::app);
  }
  else
  {
    outputText.open(inFilePath);
  }
  outputText << _OutputBuffer;
  outputText.close();
}

void TextWriter::appendText(const std::string & inAppendText)
{
  _OutputBuffer += inAppendText;
}

void TextWriter::clearText()
{
  _OutputBuffer.erase();
}
