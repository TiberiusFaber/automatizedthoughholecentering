/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Details: Interview task, create a well structured software which
 *          interprets the input arguments and call a selected corresponding
 *          image processing algorithm.
 */


#include <iostream>

#include "Utils/Types.h"
#include "Application/Application.h"

StringList convertArgumentsToStringList(char ** inStringArray, int inArraySize)
{
  StringList output;
  for (int argIdx = 0; argIdx < inArraySize; ++argIdx)
  {
    output.push_back(inStringArray[argIdx]);
  }
  output.shrink_to_fit();
  return output;
}

int main(int argc, char **argv)
{
  Application app;
  app.run(convertArgumentsToStringList(argv, argc));

  return 0;
}