/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   HoleDist
 * Details: A process. Takes the given input directory, walk through
 *          all the .png files, finds the hole center on them and
 *          write the output to a .tsv file.
 */

#ifndef PROCESS_SHOLE_DIST_H
#define PROCESS_SHOLE_DIST_H

#include "../Process.h"
#include "../../OutputWriter/TextWriter/TextWriter.h"

#include <opencv2/core.hpp>

namespace process
{

class HoleDist : public Process
{
public:
  HoleDist();
  ~HoleDist();

  // Start the execution of the process
  // Input: - inAppPath    - the path of the executable of the application
  // Input: - inParameters - parameter list of the selected process
  // Output: false if the execution had some errors
  virtual bool execute(const std::string& inAppPath, const StringList& inParameters);

private:
  // Process the given image and add the text information to the text writer
  void process(const std::string& inImagePath, const std::string& inImageName, TextWriter& outTextWriter);

  // Set the contrast and brightness of the image
  void contrastAndBrightness(cv::Mat& outImage, const float inAlpha, const float inBeta);
};

}

#endif