/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   HoleDist
 */


#include "HoleDist.h"

#include "../../Utils/Types.h"
#include "../../Utils/Console.h"
#include "../../Utils/DirectoryFunctions.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <string>

namespace process
{

HoleDist::HoleDist()
  : Process("hole_dist")
{
}


HoleDist::~HoleDist()
{
}

bool HoleDist::execute(const std::string& inAppPath, const StringList& inParameters)
{
  if (inParameters.empty())
  {
    ConsoleError("HoleDist: there is no input parameters!");
    return false;
  }

  std::string imageFolderName = inParameters[0];
  std::string folderPath = inAppPath + "\\" + imageFolderName;

  StringList images = ReadFilesOfDirectory(folderPath);

  if (images.empty())
  {
    ConsoleWarning("HoleDist: the given folder is not exists!");
    return false;
  }

  if (images.size() <= 2)
  {
    ConsoleWarning("HoleDist: the given folder is empty!");
    return false;
  }

  ConsoleNotice("Automatized Through-Hole Centering is start to process input images.");
  TextWriter tw("img_basename\tx\ty", TextWriterType::Overwrite);
  for (size_t imgIdx = 0; imgIdx < images.size(); ++imgIdx)
  {
    size_t dotPos = images[imgIdx].find_last_of('.');
    if (dotPos == std::string::npos) continue;
    if (images[imgIdx].substr(dotPos) != ".png") continue;

    process(folderPath + "\\" + images[imgIdx], images[imgIdx], tw);
  }

  std::string outputFileName = imageFolderName + ".tsv";
  tw.save(outputFileName);
  ConsoleNotice("Processing is finished. Results are saved here: " + outputFileName);

  return true;
}

void HoleDist::process(const std::string & inImagePath, const std::string& inImageName, TextWriter& outTextWriter)
{
  ConsoleNotice("HoleDist: processing image: " + inImageName);

  // Constants
  constexpr int gaussianBlurSize = 27;
  constexpr float imageDivisor = 4.0f;

  // Read image
  cv::Mat image = cv::imread(inImagePath, cv::IMREAD_GRAYSCALE);
  cv::Mat processed;

  // Resize it to make the process faster
  cv::resize(
    image,
    processed,
    cv::Size(static_cast<float>(image.cols) / imageDivisor,
      static_cast<float>(image.rows) / imageDivisor));

  cv::GaussianBlur(processed, processed, cv::Size(gaussianBlurSize, gaussianBlurSize), 0, 0);
  contrastAndBrightness(processed, 1.5f, -50.0f);

  // Find the circles
  std::vector<cv::Vec3f> circles;
  cv::HoughCircles(
    processed,
    circles,
    cv::HOUGH_GRADIENT,
    1,
    50,
    50, // param1
    18, // param2
    processed.cols / 4,  // min radius
    processed.cols / 2); // max radius

  outTextWriter.appendText("\n" + inImageName + "\t");
  if (circles.empty())
  {
    outTextWriter.appendText("-1\t-1");
    ConsoleWarning("HoleDist: hole center does not found!");
  }
  else
  {
    // For now the first circle is taken
    const int xFinal = static_cast<int>(static_cast<float>(circles[0][0]) * imageDivisor);
    const int yFinal = static_cast<int>(static_cast<float>(circles[0][1]) * imageDivisor);

    outTextWriter.appendText(std::to_string(xFinal) + "\t" + std::to_string(yFinal));
  }
  ConsoleNotice("HoleDist: processing is finished.");
}

void HoleDist::contrastAndBrightness(cv::Mat& outImage, const float inAlpha, const float inBeta)
{
  cv::Mat processed = outImage.clone();
  for (int y = 0; y < outImage.rows; y++)
  {
    for (int x = 0; x < outImage.cols; x++)
    {
      for (int c = 0; c < 3; c++)
      {
        processed.at<uchar>(y, x) = cv::saturate_cast<uchar> (inAlpha * (outImage.at<uchar>(y, x)) + inBeta);
      }
    }
  }

  outImage = processed.clone();
}

}