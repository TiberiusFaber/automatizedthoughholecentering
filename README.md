# Automatized Through-Hole Centering #

Read images from a hole taken by a microscope and detect the center of the hole.

### What is this repository for? ###

This is an interview task to Frontier Microscopy.

### How do I get set up? ###

* Open it with Visual Studio 2017
* Configure the OpenCV library
* Compile