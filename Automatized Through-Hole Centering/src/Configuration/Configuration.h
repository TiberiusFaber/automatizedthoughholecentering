/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Configuration
 * Details: Interpret the input arguments and give the information about it.
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "../Utils/Types.h"

class Configuration
{
public:
  Configuration();
  ~Configuration();

  // Interpret the input arguments and setup the parameters bellow
  // Input: inArguments - list of arguments
  // Output: true if the inerpretation was successful
  bool init(const StringList& inArguments);

  std::string getApplicationPath() const { return _ApplicationPath; }
  std::string getProcessName() const { return _ProcessName; }
  StringList getProcessparameters() const { return _ProcessParameters; }

private:
  std::string _ApplicationPath;  // The full directory path of the executable
  std::string _ProcessName;      // Name of the selected process
  StringList  _ProcessParameters; // Parameter collection of the program
};

#endif