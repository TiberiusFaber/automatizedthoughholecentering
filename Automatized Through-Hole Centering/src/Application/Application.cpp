/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Application
 */

#include "Application.h"

#include <iostream>

Application::Application()
  : _Configuration(std::make_unique<Configuration>())
  , _ProcessManager(std::make_unique<ProcessManager>())
{
}

Application::~Application()
{
}

void Application::run(const StringList& inArguments)
{
  if (!_Configuration->init(inArguments))
  {
    return;
  }

  _ProcessManager->setup();

  if (!_ProcessManager->callProcess(_Configuration->getApplicationPath(), _Configuration->getProcessName(), _Configuration->getProcessparameters()))
  {
    return;
  }
}
