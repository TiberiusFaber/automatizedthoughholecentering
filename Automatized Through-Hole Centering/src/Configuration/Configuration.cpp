/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Configuration
 */


#include "Configuration.h"

#include "../Utils/Console.h"
#include "../Utils/DirectoryFunctions.h"

Configuration::Configuration()
{
}

Configuration::~Configuration()
{
}

bool Configuration::init(const StringList& inArguments)
{
  // 0: app name
  // 1: process
  // 2...: process parameters
  if (inArguments.size() < 2)
  {
    ConsoleError("arguments are insufficient!");
    return false;
  }

  _ApplicationPath = GetApplicationPath();

  _ProcessName = inArguments[1];

  // If the process has no parameters, nothing to do, the configuration class doesn't know anything about what needs a certain process
  if (inArguments.size() == 2)
  {
    return true;
  }

  // Get the process parameters
  for (size_t argIdx = 2; argIdx < inArguments.size(); ++argIdx)
  {
    _ProcessParameters.push_back(inArguments[argIdx]);
  }
  _ProcessParameters.shrink_to_fit();

  return true;
}
