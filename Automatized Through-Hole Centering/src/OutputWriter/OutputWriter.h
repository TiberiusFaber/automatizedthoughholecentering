/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   OutputWriter
 * Details: General class to create a file output with a certain data.
 *          Interface class, need to be inherited.
 */

#ifndef OUTPUT_WRITERS_H
#define OUTPUT_WRITERS_H

#include <string>
#include <vector>
#include <memory>

class OutputWriter
{
public:
  OutputWriter() {}
  virtual ~OutputWriter() {}

  // Save the data into a selected file
  // Input: inFilePath - the file name where to save the data
  virtual void save(const std::string& inFilePath) = 0;
};

#endif