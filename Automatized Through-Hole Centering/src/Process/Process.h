/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Process
 * Details: General process class. Called by the process manager.
 *          Abstract class, needs to be inherited.
 */

#ifndef PROCESS_H
#define PROCESS_H

#include "../Utils/Types.h"

#include <string>
#include <map>
#include <memory>

namespace process
{

class Process
{
public:
  Process(const std::string& inProcessName)
  : _ProcessName(inProcessName)
  {}

  // Start the execution of the process
  // Input: - inAppPath    - the path of the executable of the application
  // Input: - inParameters - parameter list of the selected process
  // Output: false if the execution had some errors
  virtual bool execute(const std::string& inAppPath, const StringList& inParameters) = 0;

  std::string getName() const { return _ProcessName; }

private:
  std::string _ProcessName; // Name of the process
};

}

typedef std::map<std::string, std::shared_ptr<process::Process>> ProcessMap;

#endif