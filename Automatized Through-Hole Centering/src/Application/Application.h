/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Application
 * Details: Setup the program and call the selected algorithm.
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include "../Utils/Types.h"
#include "../Configuration/Configuration.h"
#include "../Process/ProcessManager/ProcessManager.h"

#include <memory>

class Application
{
public:
  Application();
  ~Application();

  // Starts the program. Setup the configuration and run the selected process.
  // Input: argument list
  void run(const StringList& inArguments);

private:
  std::unique_ptr<Configuration> _Configuration;   // Configuration manager to handle the initial data
  std::unique_ptr<ProcessManager> _ProcessManager; // Process manager for setup call the processes
};

#endif