/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * File:    Console.h
 * Details: Handles the logs.
 */

#ifndef CONSOLE_H
#define CONSOLE_H

#include <string>
#include <iostream>

// General log
static void ConsolePrint(const std::string& inLog, const std::string& inSeverity)
{
  std::cout << "[" << inSeverity << "] - " << inLog << std::endl;
}

static void ConsoleError(const std::string& inLog)
{
  ConsolePrint(inLog, "ERROR");
}

static void ConsoleWarning(const std::string& inLog)
{
  ConsolePrint(inLog, "WARNING");
}

static void ConsoleNotice(const std::string& inLog)
{
  ConsolePrint(inLog, "NOTICE");
}

#endif