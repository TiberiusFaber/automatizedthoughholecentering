/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   ProcessManager
 * Details: Initialize all the processes and call them when its needed.
 */

#ifndef PROCESS_MANAGER_H
#define PROCESS_MANAGER_H

#include "../../Utils/Types.h"
#include "../Process.h"

class ProcessManager
{
public:
  ProcessManager();
  ~ProcessManager();

  void setup();

  // Call the corresponding process and execute it
  // Input: - inAppPath     - the path of the executable of the application
  // Input: - inProcessName - the name of the executing process
  // Input: - inParameters  - parameter list of the selected process
  // Output: false if the process is not found or the process execution is failed
  bool callProcess(const std::string& inAppPath, const std::string& inProcessName, const StringList& inParameters);

private:
  ProcessMap _Processes;
};

#endif