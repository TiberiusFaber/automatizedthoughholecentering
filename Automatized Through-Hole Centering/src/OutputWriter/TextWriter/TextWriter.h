/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   TextWriter
 * Details: Take string data and write it out to a file.
 */

#ifndef TEXT_WRITERS_H
#define TEXT_WRITERS_H

#include "../OutputWriter.h"

#include <string>

enum class TextWriterType
{
  Overwrite,
  Append
};

class TextWriter : public OutputWriter
{
public:
  // Constructor
  // Input: inBuffer - initial text data
  // Input: inType   - Append (append to the saving file), Overwrite (remove the previous data of the saving file)
  TextWriter(const std::string& inBuffer = "", const TextWriterType inType = TextWriterType::Append);
  virtual ~TextWriter();

  // Save the data into a selected file
  // Input: inFilePath - the file name where to save the data
  virtual void save(const std::string& inFilePath);

  // Add text into the given buffer
  // Input: inAppendText - new information text data
  void appendText(const std::string& inAppendText);

  // Make the output buffer empty
  void clearText();

private:
  std::string _OutputBuffer;
  TextWriterType _Type;

};

#endif