/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * File:    Types.h
 * Details: Common used types.
 */

#ifndef TYPES_H
#define TYPES_H

#include <cstdint>
#include <vector>
#include <string>

typedef std::vector<std::string> StringList;

#endif