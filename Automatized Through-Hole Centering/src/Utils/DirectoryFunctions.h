/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * File:    DirectoryFunctions.h
 * Details: Algorithms to collect directory data. Currently only the Windows
 *          version is finished. For Linux it doesn't work right now.
 */

#ifndef DIRECTORY_FUNCTIONS_H
#define DIRECTORY_FUNCTIONS_H

#include "Types.h"

#include <string>
#include <vector>

#ifdef _WIN32
#include <Windows.h>
#endif

// Get the full directory path of the executable
static std::string GetApplicationPath()
{
#ifdef _WIN32
  char buffer[MAX_PATH];
  GetModuleFileNameA(NULL, buffer, MAX_PATH);
  std::string::size_type pos = std::string(buffer).find_last_of("\\/");
  return std::string(buffer).substr(0, pos);
#elif __linux__
  return "";
#endif
}

// Get all the containment in a directory
static StringList ReadFilesOfDirectory(const std::string& inPath)
{
  StringList files;

#ifdef _WIN32
  std::string pattern(inPath);
  pattern.append("\\*");
  WIN32_FIND_DATAA data;
  HANDLE hFind;
  if ((hFind = FindFirstFileA(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
    do {
      files.push_back(data.cFileName);
    } while (FindNextFileA(hFind, &data) != 0);
    FindClose(hFind);
  }
#elif __linux__
#endif

  return files;
}

#endif