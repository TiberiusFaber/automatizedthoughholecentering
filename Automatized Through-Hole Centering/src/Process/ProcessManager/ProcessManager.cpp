/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   ProcessManager
 */


#include "ProcessManager.h"

#include "../../Utils/Console.h"
#include "../HoleDist/HoleDist.h"

#include <memory>

ProcessManager::ProcessManager()
{
}


ProcessManager::~ProcessManager()
{
}

void ProcessManager::setup()
{
  std::shared_ptr<process::HoleDist> holeDist = std::make_shared<process::HoleDist>();
  _Processes[holeDist->getName()] = holeDist;
}

bool ProcessManager::callProcess(const std::string& inAppPath, const std::string & inProcessName, const StringList & inParameters)
{
  ProcessMap::iterator processIt = _Processes.find(inProcessName);
  if (processIt == _Processes.end())
  {
    ConsoleError("Process called \"" + inProcessName + "\" does not exist!");
    return false;
  }

  std::shared_ptr<process::Process> process = processIt->second;
  return process->execute(inAppPath, inParameters);
}
